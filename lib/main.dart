import 'package:calculatter/screens/first_screen.dart';
import 'package:flutter/material.dart';

import 'screens/bmi_result_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/firstScreen',
      routes:
      {
        '/firstScreen':(context)=>FirstApp(),
        '/colculaterScreen':(context)=>Calculater(Gendarx: '',age: 0,result: 0,)
      }
    );
  }
}
