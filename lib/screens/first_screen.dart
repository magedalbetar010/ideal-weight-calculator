import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'bmi_result_screen.dart';

class FirstApp extends StatefulWidget {
  @override
  int agex = _FirstAppState().age;

  _FirstAppState createState() => _FirstAppState();
}

class _FirstAppState extends State<FirstApp> {
  int age = 0;
  bool male = true;
  String Gendarx = 'Male';
  double hegiht = 120.0;
  double wegiht = 40;
  late int agex;

  void plusAge() {
    age += 3;
  }

  void eksiAge() {
    age--;
  }

  void plusWeight() {
    wegiht = wegiht + 5;
  }

  void eksiWegiht() {
    wegiht--;
  }

  void stopAge() {
    if (age < 0) {
      age = 0;
    }
  }

  void stopWegiht() {
    if (wegiht < 10) {
      wegiht = 10;
    }
  }

  String gender() {
    if (male == true) {
      return 'Male';
    } else {
      return 'Famle';
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                child: Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        child: Container(
                          decoration: BoxDecoration(
                              color:
                                  male ? Color(0xffE91E63) : Color(0xffF8BBD0),
                              borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // Image.asset('images/male.png'),
                              Text(
                                'Male',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 25),
                              ),
                              Image(
                                image: AssetImage('images/male.png'),
                                height: 150,
                                width: 150,
                                color: Colors.black,
                              )
                            ],
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            male = true;
                          });
                        },
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            male = false;
                          });
                        },
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // Image.asset('images/Famle.png',width: double.infinity/2,height: double.infinity/2,),
                              Text(
                                'FMale',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold),
                              ),
                              Image(
                                image: AssetImage('images/Famle.png'),
                                height: 150,
                                width: 150,
                              )
                            ],
                          ),
                          decoration: BoxDecoration(
                            color: male ? Color(0xffF8BBD0) : Color(0xffE91E63),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.only(
                right: 20,
                left: 20,
              ),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xffE91E63)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Height',
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          hegiht.round().toString(),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 25,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          'cm',
                          style: TextStyle(color: Colors.white, fontSize: 25),
                        ),
                      ],
                    ),
                    Slider(
                      activeColor: Color.fromARGB(255, 68, 138, 255),
                      value: hegiht,
                      onChanged: (value) {
                        setState(() {
                          hegiht = value;
                        });
                      },
                      max: 220,
                      min: 80,
                      divisions: 110,
                    )
                  ],
                ),
              ),
            )),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'AGE',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              age.toString(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                FloatingActionButton(
                                  onPressed: () {
                                    setState(() {
                                      plusAge();
                                      agex = age;
                                    });
                                  },
                                  mini: true,
                                  child: Icon(
                                    Icons.add,
                                  ),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                FloatingActionButton(
                                  onPressed: () {
                                    setState(() {
                                      eksiAge();
                                      stopAge();
                                    });
                                  },
                                  mini: true,
                                  child: Icon(
                                    Icons.remove,
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color(0xffE91E63),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Wegiht',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              wegiht.toString(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                FloatingActionButton(
                                  onPressed: () {
                                    setState(() {
                                      plusWeight();
                                    });
                                  },
                                  mini: true,
                                  child: Icon(
                                    Icons.add,
                                  ),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                FloatingActionButton(
                                  onPressed: () {
                                    setState(() {
                                      eksiWegiht();
                                      stopWegiht();
                                    });
                                  },
                                  mini: true,
                                  child: Icon(
                                    Icons.remove,
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Color(0xffE91E63)),
                        padding: EdgeInsets.all(12),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            MaterialButton(
              onPressed: () {
                gender();
                double result = wegiht / pow(hegiht / 100, 2);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Calculater(
                              result: result.roundToDouble(),
                              age: age,
                              Gendarx: gender(),
                            )));
              },
              color: Color(0xffC2185B),
              minWidth: double.infinity,
              height: 50,
              child: Text(
                'Calculate',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
